[![pipeline status](https://gitlab.com/pierlon/ap-research-project/badges/master/pipeline.svg)](https://gitlab.com/pierlon/ap-research-project/commits/master)
[![coverage report](https://gitlab.com/pierlon/ap-research-project/badges/master/coverage.svg)](https://gitlab.com/pierlon/ap-research-project/commits/master)

# AP Research Project

## Scenario

John as you may know, owns a shop on campus. John would like some assistance developing an
app that allows students from across the campus to request snacks to be delivered to their current
location. John hopes to have a rider who will deliver the snacks around campus and collect cash
from the students on his behalf. Students should be able to view a list of available snacks (name,
price, image) and make a request of one or more snacks. The request for snacks should include
their current location on campus, their name and student id. John or the delivery man should be
able to view a list of orders. While any student should be able to make a request, John or the
delivery man should have to log in to view existing orders.

## Technologies

- VueJS 2
- Laravel 5.8

## Setting up

- `composer install` to install PHP dependencies
- `yarn install` to install Node packages
- `php artisan jwt:secret` to set a JWT secret
- `yarn development` to build the frontend assets

You can then run the project with: `php artisan serve` and access it at 
[http://localhost:8000](http://localhost:8000).
