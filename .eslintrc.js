module.exports = {
    env: {
        node: true
    },
    extends: [
        'standard',
        'plugin:vue/recommended'
    ],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'vue/valid-v-on': ['error', {
          'modifiers': ['ignore']
        }]
    },
    parserOptions: {
        parser: 'babel-eslint'
    }
};
