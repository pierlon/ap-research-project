import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import auth from '@websanova/vue-auth/drivers/auth/bearer'
import http from '@websanova/vue-auth/drivers/http/vue-resource.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'

Vue.use(VueAuth, {
  auth: auth,
  http: http,
  router: router,
  fetchData: { method: 'POST' },
  refreshData: { method: 'POST', interval: 30 },
  loginData: { redirect: 'dashboard' },
  registerData: { redirect: 'dashboard' },
  logoutData: { redirect: 'login', makeRequest: true }
})
