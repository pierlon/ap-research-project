import Vue from 'vue'
import VueResource from 'vue-resource'

import { flash } from '~/utils/vuex'

Vue.use(VueResource)

Vue.http.options.root = '/api'

let token = document.head.querySelector('meta[name="csrf-token"]')

if (token) {
  Vue.http.headers.common['X-CSRF-TOKEN'] = token.content
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token')
}

Vue.http.interceptors.push((request, next) => {
  next(response => {
    switch (response.status) {
      case 401:
        if (response.body.message === 'Unauthenticated.') {
          flash('info', 'You have been logged out')
        } else {
          flash(response.body.status, response.body.message)
        }
        break
      case 403:
        flash('error', 'This action is unauthorized')
        break
      case 422:
        // TODO: make inputs available to set errors on them
        // server validation error, flash first one
        break
      case 500:
        flash('error', 'A server error occurred')
        break
      default:
        if (response.body.message && response.body.status) {
          flash(response.body.status, response.body.message)
        }
    }

    return response
  })
})
