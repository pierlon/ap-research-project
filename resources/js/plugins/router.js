import Vue from 'vue'
import { sync } from 'vuex-router-sync'

// import store from '~/store'
import router from '~/router'

// Sync store with router
sync(Vue.store, router)

Vue.router = router
