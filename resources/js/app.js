import Vue from 'vue'

import App from './App'

import './plugins'
import './components'

import '@/images/placeholder-600.png'

App.store = Vue.store
App.router = Vue.router

/* eslint-disable no-new */
new Vue({
  render: h => h(App)
}).$mount('#app')
