import Vue from 'vue'
import Meta from 'vue-meta'
import Router from 'vue-router'

import paths from './paths'

Vue.use(Meta)
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: paths,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { selector: to.hash }
    }
    return { x: 0, y: 0 }
  }
})
