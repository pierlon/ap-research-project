import Dashboard from '~/views/Dashboard'
import Login from '~/views/Login'
import Logout from '~/views/Logout'
import Order from '~/views/Order'
import Orders from '~/views/Orders'
import Register from '~/views/Register'
import Snacks from '~/views/Snacks'

export default [
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: { auth: true }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { auth: false }
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
    meta: { auth: true }
  },
  {
    path: '/order',
    name: 'Order',
    component: Order,
    meta: { auth: 'student' }
  },
  {
    path: '/orders',
    name: 'Orders',
    component: Orders,
    meta: { auth: ['owner', 'deliverer', 'student'] }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: { auth: false }
  },
  {
    path: '/snacks',
    name: 'Snacks',
    component: Snacks,
    meta: { auth: 'owner' }
  }
]
