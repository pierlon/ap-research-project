import _ from 'lodash'

export const displayErrors = (errorMessages, errors) => {
  _.each(errors, (messages, field) => {
    // Laravel likes snakes, Vue likes camels
    field = _.camelCase(field)

    errorMessages[field] = messages[0]
  })
}

export const resetErrors = errorMessages => {
  _.each(errorMessages, (message, field) => {
    errorMessages[field] = null
  })
}
