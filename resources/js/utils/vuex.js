import Vue from 'vue'

export const set = property => (state, payload) => (state[property] = payload)

export const toggle = property => state => (state[property] = !state[property])

export const flash = (type, text) => {
  Vue.store.commit('flash', { type, text })
}
