import Vue from 'vue'
import Vuex from 'vuex'

import modules from './modules'
import mutations from './mutations'
import state from './state'

Vue.use(Vuex)

// Create a new store
export default new Vuex.Store({
  modules,
  mutations,
  state
})
