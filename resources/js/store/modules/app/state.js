import sidebarImage from '@/images/sidebar.jpg'

export default {
  drawer: null,
  color: 'success',
  image: sidebarImage
}
