export default {
  flash (state, payload) {
    state.snackbar.type = payload.type
    state.snackbar.text = payload.text
    state.snackbar.enabled = true
  },

  enableSnackbar (state, value) {
    state.snackbar.enabled = value
  }
}
