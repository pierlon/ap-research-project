<?php

namespace Tests\Unit;

use App\Price;
use App\Snack;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SnackTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_it_has_a_price()
    {
        $price = 500;
        $snack = factory(Price::class)->create(compact('price'))->snack;

        $this->assertEquals($price, $snack->price);
        $this->assertInstanceOf(Price::class, $snack->prices->first());
    }

    /** @test */
    public function it_it_has_many_prices()
    {
        $snack = factory(Price::class)->create()->snack;
        $snack->prices()->create(factory(Price::class)->create()->toArray());

        $this->assertEquals(2, $snack->prices()->count());
    }

    /** @test */
    public function a_deleted_snack_should_not_exist()
    {
        $snack = factory(Price::class)->create()->snack;
        $snack->delete();

        $this->assertEquals(0, Snack::count());
    }
}
