<?php

namespace Tests\Unit;

use App\Price;
use App\Snack;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PriceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_belongs_to_a_snack()
    {
        $price = factory(Price::class)->create();

        $this->assertInstanceOf(Snack::class, $price->snack);
    }

    /** @test */
    public function the_price_is_an_integer()
    {
        $price = factory(Price::class)->create();

        $this->assertTrue(is_integer($price->price));
    }
}
