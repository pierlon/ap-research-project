<?php

namespace Tests\Feature;

use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderSnack;
use App\Price;
use App\Snack;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    /** @test */
    public function the_owner_can_view_orders()
    {
        $orders = factory(OrderSnack::class, 5)->create()->map->order;
        $ordersResponse = OrderResource::collection($orders)->response()->getData('data');

        $owner = User::whereIs('owner')->first();

        $this->actingAs($owner)
             ->get('/api/orders')
             ->assertJson($ordersResponse)
             ->assertStatus(200);
    }

    /** @test */
    public function the_owner_can_mark_an_order_as_complete()
    {
        $owner = User::whereIs('owner')->first();

        $order = factory(OrderSnack::class)->create([
            'order_id' => function () {
                return factory(Order::class)->create(['completed' => false])->id;
            },
        ])->order;

        $order->completed = true;

        $orderResponse = (new OrderResource($order))->additional([
            'status' => 'success',
            'message' => 'Order updated successfully',
        ])->response()->getData('data');

        $this->actingAs($owner)
             ->put("/api/orders/{$order->id}", ['completed' => true])
             ->assertJson($orderResponse)
             ->assertStatus(200);
    }

    /** @test */
    public function the_owner_can_delete_an_order()
    {
        $owner = User::whereIs('owner')->first();

        $order = factory(OrderSnack::class)->create()->order;

        $this->actingAs($owner)
             ->delete("/api/orders/{$order->id}")
             ->assertJson([
                 'status' => 'success',
                 'message' => 'Order deleted successfully',
             ])
             ->assertStatus(200);

        $this->assertEquals(0, Order::count());
    }

    /** @test */
    public function the_deliverer_can_view_orders()
    {
        $orders = factory(OrderSnack::class, 5)->create()->map->order;
        $ordersResponse = OrderResource::collection($orders)->response()->getData('data');

        $deliverer = User::whereIs('deliverer')->first();

        $response = $this->actingAs($deliverer)
            ->get('/api/orders')
            ->assertJson($ordersResponse);

        $response->assertStatus(200);
    }

    /** @test */
    public function the_deliverer_can_mark_an_order_as_complete()
    {
        $deliverer = User::whereIs('deliverer')->first();

        $order = factory(OrderSnack::class)->create([
            'order_id' => function () {
                return factory(Order::class)->create(['completed' => false])->id;
            },
        ])->order;

        $order->completed = true;

        $orderResponse = (new OrderResource($order))->additional([
            'status' => 'success',
            'message' => 'Order updated successfully',
        ])->response()->getData('data');

        $this->actingAs($deliverer)
            ->put("/api/orders/{$order->id}", ['completed' => true])
            ->assertJson($orderResponse)
            ->assertStatus(200);
    }

    /** @test */
    public function the_deliverer_cannot_delete_an_order()
    {
        $deliverer = User::whereIs('deliverer')->first();

        $order = factory(OrderSnack::class)->create()->order;

        $this->actingAs($deliverer)
            ->delete("/api/orders/{$order->id}")
            ->assertStatus(403);

        $this->assertEquals(1, Order::count());
    }

    /** @test */
    public function a_student_can_create_an_order()
    {
        $student = User::whereIs('student')->first();

        $order = $this->generateOrderData();

        $response = $this->actingAs($student)
                         ->postJson('api/orders', $order);

        $response->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'message' => 'Order created successfully',
            ]);

        $storedOrder = Order::with('user')->whereUserId($student->id)->latest()->first();
        $orderedSnacks = $storedOrder->snacks->keyBy('id');

        $this->assertEquals($student, $storedOrder->user);
        $this->assertEquals($order['location'], $storedOrder->location);
        $this->assertEquals(false, $storedOrder->completed);

        collect($order['selections'])->each(function ($selection) use ($orderedSnacks) {
            $orderedSnack = $orderedSnacks->get($selection['id']);

            $this->assertEquals($selection['price_id'], $orderedSnack->price_id);
            $this->assertEquals($selection['quantity'], $orderedSnack->pivot->quantity);
        });
    }

    /** @test */
    public function a_student_can_view_their_orders()
    {
        $this->markTestIncomplete('Unable to get user when Order model is booting');

        $student = User::whereIs('student')->first();

        $studentOrders = factory(OrderSnack::class, 2)->create([
            'order_id' => factory(Order::class)->create([
                'user_id' => $student->id,
            ]),
        ])->map->order;

        // create some more orders, but don't belong to the student
        factory(OrderSnack::class, 10)->create();

        $studentOrdersResponse = OrderResource::collection($studentOrders)->response()->getData('data');

        $this->actingAs($student)
             ->getJson('api/orders')
             ->assertStatus(200)
             ->assertJson($studentOrdersResponse);
    }

    /** @test */
    public function a_student_can_update_their_order()
    {
        $this->markTestSkipped('Skipped until #32 is resolved');

        $student = User::whereIs('student')->first();

        $studentOrder = factory(OrderSnack::class)->create([
            'order_id' => factory(Order::class)->create([
                'user_id' => $student->id,
            ]),
        ])->order;

        $studentOrder->location = 'foo';

        $studentOrderResponse = (new OrderResource($studentOrder))->response()->getData('data');

        $this->actingAs($student)
            ->getJson('api/orders')
            ->assertStatus(200)
            ->assertJson($studentOrderResponse);
    }

    /** @test */
    public function a_student_cannot_update_any_order()
    {
        $this->markTestSkipped('Skipped until #32 is resolved');
    }

    /** @test */
    public function a_student_can_delete_their_order()
    {
        $student = User::whereIs('student')->first();

        $order = factory(OrderSnack::class)->create([
            'order_id' => factory(Order::class)->create([
                'user_id' => $student->id,
            ]),
        ])->order;

        $this->actingAs($student)
             ->delete("api/orders/{$order->id}")
             ->assertStatus(200)
             ->assertJson([
                 'status' => 'success',
                 'message' => 'Order deleted successfully',
             ]);
    }

    /** @test */
    public function a_student_cannot_delete_any_order()
    {
        $student = User::whereIs('student')->first();

        $order = factory(OrderSnack::class)->create()->order;

        $this->actingAs($student)
            ->delete("api/orders/{$order->id}")
            ->assertStatus(403);
    }

    /** @test */
    public function a_student_cannot_complete_order_if_a_snack_is_low_in_stock()
    {
        $student = User::whereIs('student')->first();

        $orderData = $this->generateOrderData(1, false);

        $snack = Snack::find($orderData['selections'][0]['id']);

        $this->withoutExceptionHandling();

        $this->actingAs($student)
             ->postJson('/api/orders', $orderData)
             ->assertStatus(412)
             ->assertJson([
                 'status' => 'error',
                 'message' => "We don't have enough {$snack->name} in stock, please choose a lesser amount",
             ]);
    }

    private function generateOrderData(int $amount = 5, bool $withinSnackStockLimit = true)
    {
        $snackPrices = factory(Price::class, $amount)->state('in_stock')->create();

        $selections = $snackPrices->map(function (Price $price) use ($withinSnackStockLimit) {
            $snack = $price->snack;
            $quantity = $withinSnackStockLimit
                ? $this->faker->numberBetween(1, $snack->stock)
                : $snack->stock + 1;

            return [
                'id' => $price->snack_id,
                'price_id' => $price->id,
                'quantity' => $quantity,
            ];
        });

        $data = [
            'location' => $this->faker->city,
            'selections' => $selections->toArray(),
        ];

        return $data;
    }
}
