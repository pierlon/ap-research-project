<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_login()
    {
        $user = factory(User::class)->create();

        $response = $this->json('POST', '/api/auth/login', [
            'email' => $user->email,
            'password' => 'secret',
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success'])
            ->assertHeader('Authorization');
    }

    /** @test */
    public function a_user_with_incorrect_credentials_fails_login()
    {
        $user = factory(User::class)->create();

        $response = $this->json('POST', '/api/auth/login', [
            'email' => $user->email,
            'password' => 'hunter2',
        ]);

        $response
            ->assertStatus(401)
            ->assertJson([
                'status' => 'error',
                'message' => 'Invalid username or password',
            ])
            ->assertHeaderMissing('Authorization');
    }
}
