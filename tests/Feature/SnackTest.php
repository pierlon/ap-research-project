<?php

namespace Tests\Feature;

use App\Price;
use App\Snack;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SnackTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
    }

    /** @test */
    public function the_deliverer_cannot_create_a_snack()
    {
        $deliverer = User::whereIs('deliverer')->first();
        $snack = factory(Snack::class)->make()->setAppends([])->toArray();
        $snack['price'] = 500;

        $response = $this->actingAs($deliverer)
                         ->postJson('api/snacks', $snack);

        $response->assertStatus(403);

        $this->assertEquals(0, Snack::count());
    }

    /** @test */
    public function the_deliverer_can_view_snacks()
    {
        $deliverer = User::whereIs('deliverer')->first();

        $snacks = factory(Snack::class, 1)->create()->each(function ($snack) {
            $snack->prices()->create(factory(Price::class)->create()->toArray());
        });

        $response = $this->actingAs($deliverer)
                         ->getJson('api/snacks');

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => $snacks->toArray(),
                 ]);
    }

    /** @test */
    public function the_deliverer_cannot_edit_a_snack()
    {
        $deliverer = User::whereIs('deliverer')->first();
        $snack = factory(Price::class)->create()->snack;

        $snackToSave = $snack->replicate()->setAppends([])->toArray();
        $snackToSave['name'] = 'foo';

        $response = $this->actingAs($deliverer)
                         ->putJson("api/snacks/{$snack->id}", $snackToSave);

        $response->assertStatus(403);

        $this->assertNotEquals($snackToSave, Snack::find($snack->id));
    }

    /** @test */
    public function the_deliverer_cannot_delete_a_snack()
    {
        $deliverer = User::whereIs('deliverer')->first();
        $snack = factory(Price::class)->create()->snack;

        $response = $this->actingAs($deliverer)
                         ->deleteJson("api/snacks/{$snack->id}");

        $response->assertStatus(403);

        $this->assertEquals(1, Snack::count());
    }

    /** @test */
    public function the_owner_can_create_a_snack()
    {
        $owner = User::whereIs('owner')->first();
        $snack = factory(Snack::class)->make()->setAppends([])->toArray();
        $snack['price'] = 500;

        $response = $this->actingAs($owner)
                         ->postJson('api/snacks', $snack);

        $response->assertStatus(200)
                 ->assertJson([
                     'status' => 'success',
                     'message' => 'Snack created successfully',
                 ]);

        $this->assertEquals(1, Snack::count());
    }

    /** @test */
    public function the_owner_can_view_snacks()
    {
        $owner = User::whereIs('owner')->first();

        $snacks = factory(Snack::class, 1)->create()->each(function ($snack) {
            $snack->prices()->create(factory(Price::class)->create()->toArray());
        });

        $response = $this->actingAs($owner)
                         ->getJson('api/snacks');

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => $snacks->toArray(),
                 ]);
    }

    /** @test */
    public function the_owner_can_edit_a_snack()
    {
        $owner = User::whereIs('owner')->first();
        $snack = factory(Price::class)->create()->snack;

        $snackToSave = $snack->replicate()->setAppends([])->toArray();
        $snackToSave['name'] = 'foo';
        $snackToSave['price'] = 500;

        // we wait one second so the timestamp will be different, allowing
        // the new price to be returned first when retrieving from the db
        sleep(1);

        $response = $this->actingAs($owner)
                         ->putJson("api/snacks/{$snack->id}", $snackToSave);

        $response->assertStatus(200)
                 ->assertJson([
                     'status' => 'success',
                     'message' => 'Snack updated successfully',
                 ]);

        $savedSnack = Snack::find($snack->id)->makeHidden('id')->toArray();

        $this->assertEquals($snackToSave, $savedSnack);
    }

    /** @test */
    public function the_owner_can_delete_a_snack()
    {
        $owner = User::whereIs('owner')->first();
        $snack = factory(Price::class)->create()->snack;

        $response = $this->actingAs($owner)
                         ->deleteJson("api/snacks/{$snack->id}");

        $response->assertStatus(200)
                 ->assertJson([
                     'status' => 'success',
                     'message' => 'Snack deleted successfully',
                 ]);

        $this->assertEquals(0, Snack::count());
    }

    /** @test */
    public function a_student_cannot_view_snacks_not_in_stock()
    {
        $student = User::whereIs('student')->first();

        factory(Price::class)->create([
            'snack_id' => function () {
                return factory(Snack::class)->create(['stock' => 0])->id;
            },
        ]);

        $response = $this->actingAs($student)->getJson('api/snacks');

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => [],
                 ]);
    }
}
