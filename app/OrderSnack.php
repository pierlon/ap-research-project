<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderSnack extends Pivot
{
    protected $fillable = ['price_id', 'quantity'];

    public $incrementing = true;

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function snack()
    {
        return $this->belongsTo(Snack::class);
    }

    public function price()
    {
        return $this->belongsTo(Price::class);
    }
}
