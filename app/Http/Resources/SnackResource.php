<?php

namespace App\Http\Resources;

use Auth;
use Bouncer;
use Illuminate\Http\Resources\Json\JsonResource;

class SnackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $user = Auth::user();

        return [
            'id' => $this->id,
            'price_id' => $this->when(Bouncer::is($user)->a('student'), $this->price_id),
            'name' => $this->name,
            'price' => $this->price,
            'stock' => $this->stock,
            'image_url' => $this->image_url,
            'description' => $this->description,
        ];
    }
}
