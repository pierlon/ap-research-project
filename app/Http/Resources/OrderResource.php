<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'total' => $this->total,
          'location' => $this->location,
          'student' => [
              'id' => $this->user->username,
              'name' => $this->user->name,
          ],
          'status' => $this->completed ? 'Delivered' : 'In Progress',
          'created_at' => $this->created_at->format('F d, Y \a\t h:i A'),
          'completed_at' => $this->completed ? $this->completed_at->format('F d, Y \a\t h:i A') : '-',
          'snacks' => OrderedSnackResource::collection($this->snacks),
        ];
    }
}
