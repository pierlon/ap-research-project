<?php

namespace App\Http\Controllers;

use App\Http\Resources\SnackResource;
use App\Snack;
use Exception;
use Illuminate\Http\Request;

class SnackController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Snack::class);
    }

    /**
     * Return a listing of snacks.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $snacks = Snack::all();

        return SnackResource::collection($snacks);
    }

    /**
     * Store a newly created snack in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:snacks,name',
            'image' => 'sometimes|nullable|image',
            'stock' => 'required|integer',
            'price' => 'required|integer',
            'description' => 'required|string',
        ]);

        $snack = Snack::create($request->except('price'));

        $snack->prices()->create($request->only('price'));

        return [
            'status' => 'success',
            'message' => 'Snack created successfully',
        ];
    }

    /**
     * Update the specified snack in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Snack               $snack
     *
     * @return array
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Snack $snack)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'image' => 'sometimes|nullable|image',
            'stock' => 'required|integer',
            'price' => 'required|integer',
            'description' => 'required|string',
        ]);

        $snack->update($request->except('price'));

        // create a new price if it has changed
        if (($price = $request->get('price')) !== $snack->price) {
            $snack->prices()->create(compact('price'));
        }

        return [
            'status' => 'success',
            'message' => 'Snack updated successfully',
        ];
    }

    /**
     * Remove the specified snack from storage.
     *
     * @param \App\Snack $snack
     *
     * @return array
     * @throws \Exception
     */
    public function destroy(Snack $snack)
    {
        try {
            $snack->delete();
        } catch (Exception $exception) {
            return response([
                'status' => 'error',
                'message' => $exception->getMessage(),
            ], 412);
        }

        return [
            'status' => 'success',
            'message' => 'Snack deleted successfully',
        ];
    }
}
