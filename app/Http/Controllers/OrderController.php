<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Order;
use App\Snack;
use Auth;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Order::class);
    }

    /**
     * Display a listing of orders.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // TODO: test for verifying student can only see their orders
        $orders = Order::all();

        return OrderResource::collection($orders);
    }

    /**
     * Store a newly created order in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'location' => 'required|string|max:255',
            'selections' => 'required|array',
        ]);

        // create an order for authenticated user
        $order = Auth::user()->orders()->create($request->only('location'));

        // store snack selections onto order
        $selections = $this->mapSelections($request->get('selections'));

        // lock ordered snacks for updating the stock
        $orderedSnacks = Snack::lockForUpdate()->find(array_keys($selections));

        // make sure each snack is in stock
        foreach ($orderedSnacks as $snack) {
            if ($snack->stock < $selections[$snack->id]['quantity']) {
                return response([
                    'status' => 'error',
                    'message' => "We don't have enough {$snack->name} in stock, please choose a lesser amount",
                ], 412);
            }

            // update snack's stock
            $snack->stock -= $selections[$snack->id]['quantity'];
        }

        // save after making sure every snack is in stock
        $orderedSnacks->each->save();

        $order->snacks()->attach($selections);

        return [
            'status' => 'success',
            'message' => 'Order created successfully',
        ];
    }

    /**
     * Update the specified order in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Order               $order
     *
     * @return OrderResource
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Order $order)
    {
        $this->validate($request, [
            'completed' => 'required|bool',
        ]);

        $order->update($request->only('completed'));

        return (new OrderResource($order))->additional([
            'status' => 'success',
            'message' => 'Order updated successfully',
        ]);
    }

    /**
     * Remove the specified order from storage.
     *
     * @param \App\Order $order
     *
     * @return array
     *
     * @throws \Exception
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return [
            'status' => 'success',
            'message' => 'Order deleted successfully',
        ];
    }

    private function mapSelections(array $snacks)
    {
        return collect($snacks)->mapWithKeys(function ($snack) {
            return [
                $snack['id'] => [
                    'price_id' => $snack['price_id'],
                    'quantity' => $snack['quantity'],
                ],
            ];
        })->toArray();
    }
}
