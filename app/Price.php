<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $casts = [
        'price' => 'integer',
    ];

    protected $fillable = ['price'];

    protected $hidden = ['snack_id', 'created_at'];

    public function snack()
    {
        return $this->belongsTo(Snack::class);
    }

    /**
     * Update the creation timestamp.
     */
    protected function updateTimestamps()
    {
        $time = $this->freshTimestamp();

        if (!$this->exists && !is_null(static::CREATED_AT) &&
            !$this->isDirty(static::CREATED_AT)) {
            $this->setCreatedAt($time);
        }
    }
}
