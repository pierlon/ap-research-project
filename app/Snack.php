<?php

namespace App;

use Auth;
use Bouncer;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Snack extends Model
{
    protected $appends = ['price'];

    protected $attributes = [
        'image_url' => '/images/placeholder-600.png',
    ];

    protected $casts = [
        'stock' => 'integer',
    ];

    protected $fillable = ['name', 'stock', 'image_url', 'description'];

    protected $hidden = ['created_at', 'updated_at', 'prices'];

    protected $with = ['prices'];

    protected static function boot()
    {
        parent::boot();

        // Bouncer doesn't like nulls so we create a user to quell its fears
        $user = Auth::user() ?? new User();

        if (Bouncer::is($user)->a('student')) {
            self::addGlobalScope('in_stock', function (Builder $builder) {
                $builder->where('stock', '>', 0);
            });
        }
    }

    /**
     * Get the latest price for a snack.
     *
     * @return Price
     */
    public function getPriceAttribute()
    {
        return $this->attributes['price'] = $this->prices()->first()->price;
    }

    public function getPriceIdAttribute()
    {
        return $this->attributes['price_id'] = $this->prices()->first()->id;
    }

    public function prices()
    {
        return $this->hasMany(Price::class)->latest();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)->using(OrderSnack::class);
    }

    public function delete()
    {
        if ($this->orders()->doesntExist()) {
            $this->prices()->delete();
            return parent::delete();
        }

        throw new Exception('There are orders for this snack');
    }
}
