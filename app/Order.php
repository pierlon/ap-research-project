<?php

namespace App;

use Auth;
use Bouncer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $appends = ['completed'];

    protected $fillable = ['location', 'completed'];

    protected $casts = [
        'completed_at' => 'datetime',
    ];

    protected $with = ['snacks', 'user'];

    protected static function boot()
    {
        parent::boot();

        // Bouncer doesn't like nulls so we create a user to quell its fears
        $user = Auth::user() ?? new User();

        if (Bouncer::is($user)->a('student')) {
            self::addGlobalScope('own', function (Builder $builder) use ($user) {
                $builder->whereUserId($user->id);
            });
        }
    }

    public function delete()
    {
        // delete all snack orders
        $this->snacks()->sync([]);

        return parent::delete();
    }

    public function snacks()
    {
        return $this->belongsToMany(Snack::class)
                    ->using(OrderSnack::class)
                    ->withPivot('quantity')
                    ->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCompletedAttribute()
    {
        return !is_null($this->completed_at);
    }

    public function setCompletedAttribute(bool $completed)
    {
        $this->attributes['completed_at'] = $completed ? Carbon::now() : null;
    }

    public function getTotalAttribute()
    {
        $total = 0;

        foreach ($this->snacks as $snack) {
            $total += ($snack->price * $snack->pivot->quantity);
        }

        return $total;
    }
}
