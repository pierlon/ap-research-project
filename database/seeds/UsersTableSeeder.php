<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // John aka owner of John Shop
        $owner = factory(User::class)->create([
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'john@shop.com',
        ]);

        Bouncer::assign('owner')->to($owner);

        // Neil aka deliverer of snacks for John Shop
        $deliverer = factory(User::class)->create([
            'first_name' => 'Neil',
            'last_name' => 'Armstrong',
            'email' => 'neil@shop.com',
        ]);

        Bouncer::assign('deliverer')->to($deliverer);

        // other users (students)
        factory(User::class, 10)->state('student')->create()->each(function (User $user) {
            Bouncer::assign('student')->to($user);
        });
    }
}
