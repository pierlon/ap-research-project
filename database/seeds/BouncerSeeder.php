<?php

use App\Order;
use App\Snack;
use Illuminate\Database\Seeder;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Bouncer::allow('owner')->toManage(Snack::class);
        Bouncer::allow('owner')->to(['view', 'update', 'delete'], Order::class);

        Bouncer::allow('deliverer')->to(['view', 'update'], Order::class);

        Bouncer::allow('student')->toOwn(Order::class);
        Bouncer::allow('student')->to('create', Order::class);
        Bouncer::allow('student')->to('view', Snack::class);
    }
}
