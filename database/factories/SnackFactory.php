<?php

use App\Snack;
use Faker\Generator as Faker;

$factory->define(Snack::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'stock' => $faker->numberBetween(0, 50),
        'description' => $faker->sentence,
    ];
});

$factory->state(Snack::class, 'with_image', function (Faker $faker) {
    return [
        'image_url' => $faker->imageUrl(640, 480, 'food'),
    ];
});
