<?php

use App\Order;
use App\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $completed = $faker->boolean;

    return [
        'user_id' => function () {
            return factory(User::class)->state('student')->create()->id;
        },
        'location' => $faker->city,
        'completed_at' => $completed ? now() : null,
    ];
});
