<?php

use App\Order;
use App\OrderSnack;
use App\Price;
use Faker\Generator as Faker;

$factory->define(OrderSnack::class, function (Faker $faker) {
    $price = factory(Price::class)->create();

    return [
        'order_id' => function () {
            return factory(Order::class)->create()->id;
        },
        'snack_id' => $price->snack_id,
        'price_id' => $price->id,
        'quantity' => $faker->numberBetween(1, $price->snack->stock),
    ];
});
