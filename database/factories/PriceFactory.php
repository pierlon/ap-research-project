<?php

use App\Price;
use App\Snack;
use Faker\Generator as Faker;

$factory->define(Price::class, function (Faker $faker) {
    return [
        'snack_id' => function () {
            return factory(Snack::class)->create()->id;
        },
        'price' => $faker->randomNumber(3, true),
    ];
});

$factory->state(Price::class, 'in_stock', function (Faker $faker) {
    return [
        'snack_id' => function () use ($faker) {
            return factory(Snack::class)->create([
                'image_url' => 'images/placeholder-600.png',
                'stock' => $faker->numberBetween(1, 50),
            ])->id;
        },
    ];
});
