<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSnackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_snack', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('snack_id');
            $table->unsignedBigInteger('price_id');

            $table->integer('quantity');

            $table->foreign('order_id')
                  ->references('id')->on('orders');

            $table->foreign('snack_id')
                  ->references('id')->on('snacks');

            $table->foreign('price_id')
                  ->references('id')->on('prices');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_snack');
    }
}
