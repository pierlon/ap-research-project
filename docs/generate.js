const plantuml = require('node-plantuml')
const fs = require('fs')

if (!fs.existsSync('docs/out')) {
  fs.mkdirSync('docs/out')
}

var puml = fs.readFileSync('docs/erd.puml', 'utf8')

const output = plantuml.generate(puml)
output.out.pipe(fs.createWriteStream('docs/out/erd.png'))
